﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VinCity.CMS.Common;
using VinCity.Models.Entities;
using VinCtity.CMS.Repository;

namespace VinCity.CMS.Controllers
{
    public class HomeController : Controller
    {
        private VinCityEntities db = new VinCityEntities();
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        public ActionResult Index()
        {

            List<Article> listArticle = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 1).OrderByDescending(o => o.Id).Take(4).ToList();
            if (listArticle.Count > 4)
            {
                listArticle.RemoveRange(0, 4);
            }
            ViewBag.ArticleOlder = listArticle;

            List<Article> listArticleProject = db.Articles.Where(x => x.Status == (int)CMS_STATUS.ACTIVE && x.CategoryId == 2).OrderByDescending(o => o.Id).Take(10).ToList();
            ViewBag.listProject = listArticleProject;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}